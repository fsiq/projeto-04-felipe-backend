const { verifyToken, decryptToken } = require('../cryptograph.utils')
const { emailAlreadyExist, validatePermission } = require('../../services/user.service')

const Authorize =  (route = '*') => {
    return async (req, res, next) => {
        const { token } = req.headers;

        if (!token) {
            return res.status(403).send({
                message: "usuário não autorizado."
            })
        }

        if(!verifyToken(token)) {
            return res
                .status(401)
                .send({ mensagem: "usuário não autenticado 2332." })
        }

        const { id, email, kindUser } = decryptToken(token)
        if (!(await emailAlreadyExist(email))) {
            return res
                .status(403)
                .send({ message: "usuário não autenticado." })
        }

        if (route != '*') {
            if (!validatePermission(kindUser, route)) {
                return res.status(403).send({ message: "usuário não autorizado." })
            }
        }

        req.user = {
            id,
            email,
            kindUser,
        }

        return next();
    }
}

module.exports = Authorize
