const sandgrid = require('@sendgrid/mail')
sandgrid.setApiKey(process.env.SANDGRID_API_KEY)

const sendMessage = async ({destinatario, remetente, assunto, corpo}) => {
    const msg = {
        to: destinatario,
        from: remetente,
        subject: assunto,
        text: corpo,
    }
    
    await sandgrid
        .send(msg)
    console.log('E-MAIL ENVIADO')
}

module.exports = {
    sendMessage
}