const { cliente } = require('../models/index')

const { emailAlreadyExist } = require('../services/user.service')
const { createHash } = require('../utils/cryptograph.utils')
const { toUserDTO } = require('../mapper/cliente.mapper')
const { removeFile, moveFile } = require('../utils/file.utils')

const createNewCliente = async (model) => {
    const {name, email , password, ...rest} = model

    if(await emailAlreadyExist(email))
        return{
            sucsses: false,
            message: 'operação não pode ser realizada',
            details: [
                'Já existe usuário cadastrado para o email informado'
            ]
        }
    const newCliente = await cliente.create({
        name,
        email,
        password: createHash(password),
        status: "Ativo"
    })
    return {
        sucsses: true,
        message: 'Operação realizada com sucesso',
        data: {
            ...toUserDTO(newCliente)
        }
    }
}

const listAllCliente = async () => {
    const listUsersDB = await cliente.find({ kind: 'cliente'})
    return listUsersDB.map(listUsersDB => {
        return toUserDTO(listUsersDB)
    })
}

const listClienteByid = async (id) => {
    const userDB = await cliente.findById(id)
    if (userDB) {
        return toUserDTO(userDB)
    }
    return
}

const updateClienteService = async (id, model) => {
    const clienteDB = await cliente.findOne({ _id: id})

    if (!clienteDB) {
        return {
            sucsses: false,
            message: 'não foi possível realizar a operação',
            details: [
                '"id" da parceiro não existe.'
            ]
        }
    }

    clienteDB.name = model.name
    clienteDB.email = model.email
    clienteDB.phone = model.phone
    clienteDB.country = model.country
    clienteDB.state = model.state
    clienteDB.city = model.city
    clienteDB.address = model.address
    clienteDB.zip = model.zip
    clienteDB.status = model.status

    if(model.image) {
        removeFile('profile', clienteDB.image.name)
        moveFile(model.image.initialDestination, model.image.newDestination)
        clienteDB.image = {
            initialName: model.image.initialName,
            name: model.image.newName,
            type: model.image.type
        }
    }
    await clienteDB.save()

    return {
        sucsses: true,
        message: 'Operação realizada com sucesso',
        data: {
            ...toUserDTO(clienteDB)
        }
    }

}

const removeClienteService = async (id) => {
    const clientDB = await cliente.findById({ _id: id })
    if(!clientDB){
        return {
            sucsses: false,
            message: 'não foi possível realizar a operação',
            details: [
                '"id" do cliente não existe.'
            ]
        }
    }
    const { image } = clientDB
    removeFile('profile', image.name)

    await cliente.deleteOne(clientDB)
    return {
        sucsses: true,
        message: 'Operação realizada com sucesso.'
    }
}

module.exports = {
    createNewCliente,
    listAllCliente,
    listClienteByid,
    updateClienteService,
    removeClienteService
}