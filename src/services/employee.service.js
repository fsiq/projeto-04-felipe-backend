const { employee, category, partner } = require ('../models/index')
const { toDTO, toItemListDTO } = require('../mapper/employee.mapper')
const { emailAlreadyExist } = require('../services/user.service')
const cryptograph = require('../utils/cryptograph.utils')
const fileUtils = require('../utils/file.utils')

const { moveFile, removeFile } = fileUtils
const { createHash } = cryptograph

const listAllEmployeeService = async () => {
    const listAllEmployeeDB = await employee.find({}).populate("partnersId", "categoryId")
    return listAllEmployeeDB.map(employeeDB => {
        return toDTO(employeeDB)
    })
}

const listEmployeeByIdService = async (id) => {
    const employeeDB = await employee.findById(id).populate("partnersId", "categoryId")
    if (employeeDB) {
        return toDTO(employeeDB)
    }
    return
}

const findByFilter = async (filter) => {
    const filterMongo = {}

    if (filter.categoryId) {
        filterMongo.categoryId = filter.categoryId
    }
    if(filter.partnerId) {
        filterMongo.partnersId = filter.partnerId
    }
    if(filter.nameLike) {
        filterMongo.name = { $regex: '.*' + filter.nameLike + '.*'}
    }
    
    const resultDB = await employee.find(filterMongo).populate("partnersId", "categoryId")

    return resultDB.map(item => {
        return toItemListDTO(item)
    })

}

const createEmployeeService = async (model) => {
    console.log(model)
    const { name, email, password, cnpj, ...rest } = model

    const [categoryDB, partnerDB] = await Promise.all([
        category.findById(model.categoryId),
        partner.findById(model.id)
    ])
    
    if(!partnerDB) {
        return {
            sucesso: false,
            message: 'operação não pode ser realizada',
            details: [
                'Não existe Parceiro cadastrado para o id informado'
            ]
        };
    }

    if(!categoryDB) {
        return {
            sucesso: false,
            message: 'operação não pode ser realizada',
            details: [
                'Não existe Categoria cadastrado para o id informado'
            ]
        };
    }

    const newEmployee =  await employee.create({
        name,
        email,
        categoryId: model.categoryId,
        partnersId: model.id,
        ...rest,
        password: createHash(password),
        status: "Ativo"
    })

    categoryDB.employees = [...categoryDB.employees, newEmployee._id];
    partnerDB.employees = [...partnerDB.employees, newEmployee._id];

    await Promise.all([
        categoryDB.save(),
        partnerDB.save()
    ])

    return {
        sucsses: true,
        message: 'Operação realizada com sucesso',
        data: {
            id: newEmployee._id,
            name: newEmployee.name
        }
    }
}

const deleEmployeeService = async (id, partnerID, userId ) => {
    const [employeeDB, partnerDB] = await Promise.all([
        employee.findById(id),
        partner.findById(partnerID)
    ])

    if(!employeeDB){
        return {
            sucsses: false,
            message: 'não foi possível realizar a operação',
            details: [
                '"id" da employee não existe.'
            ]
        }
    }

    if(partnerID !== userId) {
        return {
            sucsses: false,
            message: 'não foi possível realizar a operação',
            details: [
                'O employee a ser excluido não pertence ao fornecedor.'
            ]
        }
    }

    if(!partnerDB){
        return {
            sucsses: false,
            message: 'não foi possível realizar a operação',
            details: [
                '"id" da partner não existe.'
            ]
        }
    }

    if(!employeeDB.partnersId.toString() === partnerID) {
        return {
            sucsses: false,
            message: 'não foi possível realizar a operação',
            details: [
                'O partner informado é inválido.'
            ]
        }
    }
    
    const categoryDB = await category.findById(employeeDB.categoryId)
    
    categoryDB.employees = categoryDB.employees.filter(item => {
        return item.toString() !== id
    })

    partnerDB.employees = partnerDB.employees.filter(item => {
        return item.toString() !== id
    })

    
    const { image } = employeeDB
    removeFile('profile', image.name)

    await Promise.all([
        categoryDB.save(),
        partnerDB.save(),
        employeeDB.deleteOne(employeeDB)
    ])

    return {
        sucsses: true,
        message: 'Operação realizada com sucesso.'
    }
}

module.exports = {
    listAllEmployeeService,
    listEmployeeByIdService,
    createEmployeeService,
    findByFilter,
    deleEmployeeService,
}