const  { partner, employee } = require('../models/index')
const { toListItemDTO } = require('../mapper/partners.mapper')
const { emailAlreadyExist, findKindUserById } = require('../services/user.service')
const { createHash } = require('../utils/cryptograph.utils')
const { sendMessage } = require('../utils/email.utils')
const { removeFile, moveFile } = require('../utils/file.utils')

const cnpjAlreadyExist = async (cnpj) => {
    const result = await partner.find({
        cnpj
    })
    return result.length > 0 ? true : false
}

const createNewPartner =  async (model) => {
    const { email, password, cnpj, ...rest } = model

    //validate cnpj
    if(await cnpjAlreadyExist(cnpj))
        return{
            sucsses: false,
            message: 'operação não pode ser realizada',
            details: [
                'Já existe fornecedor cadastrado para o cnpj informado'
            ]
        }
    
    if(await emailAlreadyExist(email))
        return{
            sucsses: false,
            message: 'operação não pode ser realizada',
            details: [
                'Já existe usuário cadastrado para o email informado'
            ]
        }
    
    const newPartner = await partner.create({
        email,
        cnpj,
        ...rest,
        password: createHash(password),
        status: "Analise"
    })

    return {
        sucsses: true,
        message: 'Operação realizada com sucesso',
        data: {
            ...toListItemDTO(newPartner)
        }
    }
}

const listAllPartnerService = async (filter) => {
    const resultDB = await partner.find().sort({ name: 1, status: 1 });

    return resultDB.map(item => {
        return toListItemDTO(item)
    })
}

const listPartnerById = async (id, userId, kind) => {
    const partnerDB = await partner.findById(id)

    if(!partnerDB) {
        return {
            sucsses: false,
            message: "operação não pode ser realizada",
            details: [
                "o partner pesquisado não existe"
            ]
        }
    }
    const kindUser = findKindUserById(kind)

    if(kindUser.description === "partner") {
        if(id !== userId) {
            return {
                sucsses: false,
                message: "Operação não pode ser realizada.",
                details: [
                    "o usuário não pode realizar esta operação"
                ]
            }
        }
    }

    return {
        sucsses: true,
        data: toListItemDTO(partnerDB)
    }
}

const changeStatus = async (id, status) => {
    const resultDB = await partner.findById(id)
    
    if(!resultDB) {
        return{
            sucsses: false,
            message: 'operação não pode ser realizada',
            details: [
                'Desculpe não foi encontrado um parçeiro com esse id'
            ]
        }
    }
    resultDB.status = status

    await resultDB.save()

    if(status === 'Ativo') {
        sendMessage({
            destinatario: resultDB.email,
            remetente: process.env.SANDGRID_REMETENTE,
            assunto: `Confirmação do cadastro de ${resultDB.companyName}`,
            corpo: `Sua conta de parceiro do hathor já esta liberada para uso.`
        })
    }

    return {
        sucsses: true,
        message: 'Operacão realizada com sucesso!',
        data: {
            ...toListItemDTO(resultDB)
        }
    }
}

const updatePartnerService = async (id, model) => {
    const partnerDB = await partner.findOne({ _id: id })

    if (!partnerDB) {
        return {
            sucsses: false,
            message: 'não foi possível realizar a operação',
            details: [
                '"id" da parceiro não existe.'
            ]
        }
    }

    partnerDB.name = model.name
    partnerDB.email = model.email
    partnerDB.companyName = model.companyName
    partnerDB.cnpj = model.cnpj
    partnerDB.accountable = model.accountable
    partnerDB.phone = model.phone
    partnerDB.uf = model.uf
    partnerDB.city = model.city
    partnerDB.address = model.address
    partnerDB.status = model.status

    if(model.image) {
        removeFile('profile', partnerDB.image.name)
        moveFile(model.image.initialDestination, model.image.newDestination)
        partnerDB.image = {
            initialName: model.image.initialName,
            name: model.image.newName,
            type: model.image.type
        }
    }
    
    await partnerDB.save()

    if(model.status === 'Ativo') {
        sendMessage({
            destinatario: partnerDB.email,
            remetente: process.env.SANDGRID_REMETENTE,
            assunto: `Confirmação do cadastro de ${partnerDB.companyName}`,
            corpo: `Sua conta de parceiro do hathor já esta liberada para uso.`
        })
    }

    return {
        sucsses: true,
        message: 'Operação realizada com sucesso',
        data: {
            ...toListItemDTO(partnerDB)
        }
    }
}

module.exports = {
    createNewPartner,
    listAllPartnerService,
    listPartnerById,
    changeStatus,
    updatePartnerService,
}
