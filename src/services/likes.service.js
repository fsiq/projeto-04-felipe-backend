const {partner, cliente, like } = require('../models/index')

const createLike = async (partnerId, userId) => {
    const [partnerDB, clienteDB] = await Promise.all([
        partner.findById(partnerId),
        cliente.findById(userId)
    ])

    if(!partnerDB) {
        return{
            sucsses: false,
            message: "operação não pode ser realizada",
            details: [
                'o partner pesquisado não existe'
            ]
        }
    }

    const likeDB = await like.create({
        partner: partnerId,
        cliente: userId
    })

    partnerDB.likes = [...partnerDB.likes, likeDB._id]
    clienteDB.likes = [...partnerDB.likes, likeDB._id]

    await Promise.all([
        partnerDB.save(),
        clienteDB.save()
    ])

    return{
        sucsses: true,
        data: {
            id: likeDB._id,
            partner: partnerDB.companyName,
            cliente: clienteDB.name
        }
    }
}

module.exports = {
    createLike,
}