const { user } = require('../models/index')
const cryptograph = require('../utils/cryptograph.utils')
const userMapper = require('../mapper/user.mapper')

const { toUserDTO } = userMapper

const profiles = [
    {
        id: 1,
        description: 'admin',
        permissions: [
            'CREATE_CATEGORY',
            'SEARCH_PARTNER_ID'
        ]
    },
    {
        id: 2,
        description: 'partner',
        permissions: [
            'DELETE_EMPLOYEEE',
            'SEARCH_PARTNER_ID'
        ]
    },
    {
        id: 4,
        description: 'cliente',
        permissions: [
            'CREATE_LIKE'
        ]
    },
]

const findKindUserById = (kindUserId) => {
    return profiles.find(item => {
        return item.id === kindUserId
    })
}

const emailAlreadyExist = async (email) => {
    const users = await user.find({ email })
    return users.length > 0 ? true : false
}

const userAlreadyExist = async (email, password) => {
    return await user.findOne({ email, password: cryptograph.createHash(password) }) ? true : false
}

const createCredential = async (email) => {
    const userDB = await user.findOne({
        email,
    })
    const userDTO = userMapper.toUserDTO(userDB)
    return {
        token: cryptograph.createToken(userDTO),
        userDTO,
    }
}

const validate = async (email, password) => {
    const resultDB = await userAlreadyExist(email, password)
    if(!resultDB) {
        return {
            sucsses: false,
            message: "não foi possivel autenticar o usuario",
            details: [
                "usuário ou senha inválido",
            ],
        }
    }
    return {
        sucsses: true,
        message: "Usuário autenticado com sucesso",
        data: await createCredential(email)
    }
}

//TODO :  REMOVER DO SERVICO DE USUARIOS
const getProfileById = (profileId) => {
    const result = profiles.find(item => Number(item.id) === Number(profileId))
    return result
}



const validatePermission = async (profileId,permission) => {
    const profile = getProfileById(profileId);
    const result = profile.permissions.includes(permission)
    return result
}


module.exports = { 
    userAlreadyExist,
    validate,
    emailAlreadyExist,
    validatePermission,
    findKindUserById
}