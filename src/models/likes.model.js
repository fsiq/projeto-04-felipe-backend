const mongoose = require('mongoose')
const Schema = mongoose.Schema

const likeSchema = {
    partner: {
        type: Schema.Types.ObjectId,
        ref: 'partner',
        required: true,
    },
    cliente: {
        type: Schema.Types.ObjectId,
        ref: 'cliente',
        required: true,
    }
}

module.exports = likeSchema