const mongoose = require('mongoose')
const Schema = mongoose.Schema

const clienteSchema = {
    country: {
        type: String,
        required: false,
    },
    state: {
        type: String,
        required: false,
    },
    city: {
        type: String,
        required: false,
    },
    address:{
        type: String,
        required: false,
    },
    zip: {
        type: Number,
        required: false,
    },
    phone: {
        type: String,
        required: false,
    },
    status: {
        type: String,
        required: false,
    },
    likes: [{
        type: Schema.Types.ObjectId,
        ref: 'like'
    }]
}

module.exports = clienteSchema;