const { required } = require('joi');
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const employeeSchema = {
    state: {
        type: String,
        required: true,
    },
    city: {
        type: String,
        required: true,
    },
    street:{
        type: String,
        required: true,
    },
    zip: {
        type: Number,
        required: true,
    },
    phone: {
        type: String,
        required: true,
    },
    partnersId: {
        type: Schema.Types.ObjectId,
        ref: 'partner',
        required: true
    },
    categoryId: [{
        type: Schema.Types.ObjectId,
        ref: 'category',
        required: true
    }],
    events: [{
        type: Schema.Types.ObjectId,
        ref: 'event'
    }],
    status: {
        type: String,
        required: false,
    }
}

module.exports = employeeSchema;