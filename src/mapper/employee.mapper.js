const fileUtils = require('../utils/file.utils')
const { createAddressDownload } = fileUtils

const toDTO = (model) => {
    const {_id, name, email, image, phone, status, address, partnersId } = model
    return {
        id: _id,
        name,
        email,
        phone,
        address,
        partnersId,
        image: createAddressDownload('profile', image.name),
        status
    }
}
const toItemListDTO = (model) => {
    const {_id, name, email, phone, image, createdAt, status, partnersId, categoryId} = model

    return {
        id: _id,
        name,
        email,
        phone,
        partnersID: partnersId ? partnersId._id : null,
        categoryId,
        image: createAddressDownload('profile', image.name),
        createdAt,
        status
    }
}

module.exports = {
    toDTO,
    toItemListDTO
}