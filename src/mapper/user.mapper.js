const fileUtils = require('../utils/file.utils')
const { createAddressDownload } = fileUtils

const KindListUser = (kind) => {
    switch (kind) {
        case "admin":
            return 1;
        case "partner":
            return 2;
        case "employee":
            return 3;
        case "cliente":
            return 4;
        default:
            break;
    }
}

const toUserDTO = (model) => {
    const { id, email, kind, name, companyName, image} = model;
    return {
        id,
        email,
        name: name ? name : companyName,
        kindUser: KindListUser(kind),
        image: createAddressDownload('profile', image.name)
    }
}

module.exports = { 
    toUserDTO
}