const fileUtils = require('../utils/file.utils')
const { createAddressDownload } = fileUtils

const toUserDTO = (model) => {
    const { _id, name, email, status, country, state, city, address, zip, phone, image} = model
    return {
        id: _id,
        name,
        email,
        country,
        state,
        city,
        address,
        zip,
        status,
        phone,
        image: createAddressDownload('profile', image.name)
    }
}

module.exports = {
    toUserDTO
}

