const fileUtils = require('../utils/file.utils')
const { createAddressDownload } = fileUtils

const toDTO = (model) => {
    return {
 
    }
}

const toListItemDTO = (model) => {
    const { _id, name, companyName, email, accountable, phone, cnpj, uf, city, address, image, status, zip} = model
    return {
        id: _id,
        name,
        companyName,
        accountable, 
        phone,
        email,
        cnpj,
        uf,
        city,
        zip,
        address,
        image: createAddressDownload('profile', image.name),
        status
    }
}

module.exports = {
    toDTO,
    toListItemDTO
}