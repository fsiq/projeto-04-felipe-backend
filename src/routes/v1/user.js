const userCTRL = require ('../../controllers/user.ctrl')
const clienteCTRL = require ('../../controllers/cliente.ctrl')
const fileUploadMiddleware = require('../../utils/middlewares/file-upload.middleware')
const  validateDTO = require('../../utils/middlewares/validate-dto.middleware')

const joi = require('joi')
const { getAllCliente, signUpCliente, getClienteByid, updateCliente, removeCliente} = clienteCTRL
const { auth } = userCTRL

module.exports = (Router) => {
    Router
        .route('/auth')
        .post(
            validateDTO('body', {
                email: joi.string().required().messages({
                    'any.required': `"e-mail" é um campo obrigatório`,
                    'string.empty': `"e-mail" não deve ser vazio`,
                }),
                password: joi.string().required().messages({
                    'any.required': `"senha" é um campo obrigatório`,
                    'string.empty': `"senha" não deve ser vazio`,
                }),
            }),
            auth
        )

    Router
        .route('/users')
        .get(getAllCliente)
        .post(
            validateDTO('body', {
                name: joi.string().required().messages({
                    'any.required': `"nome" é um campo obrigatório`,
                    'string.empty': `"nome" não deve ser vazio`,
                }),
                email: joi.string().required().messages({
                    'any.required': `"e-mail" é um campo obrigatório`,
                    'string.empty': `"e-mail" não deve ser vazio`,
                }),
                password: joi.string().required().messages({
                    'any.required': `"senha" é um campo obrigatório`,
                    'string.empty': `"senha" não deve ser vazio`,
                }),
            }),
            signUpCliente
        )
    
    Router
        .route('/users/:id')
        .get(getClienteByid)
        .put(
            fileUploadMiddleware('profile', true),
            validateDTO('params', {
                id: joi.string().regex(/^[0-9a-fA-F]{24}$/).required().messages({
                    'any.required': `"categoria id" é um campo obrigatório`,
                    'string.empty': `"categoria id" não deve ser vazio`,
                    'string.regex': `"categoria id" fora do formato experado`,
                }),   
            }),
            validateDTO('body', {
                name: joi.string().required().messages({
                    'any.required': `"Nome" é um campo obrigatório`,
                    'string.empty': `"Nome" não deve ser vazio`
                }),
                email: joi.string().required().messages({
                    'any.required': `"email" é um campo obrigatório`,
                    'string.empty': `"email" não deve ser vazio`
                }),
                country: joi.string().required().messages({
                    'any.required': `"país" é um campo obrigatório`,
                    'string.empty': `"país" não deve ser vazio`
                }),
                state: joi.string().required().messages({
                    'any.required': `"estado" é um campo obrigatório`,
                    'string.empty': `"estado" não deve ser vazio`
                }),
                city: joi.string().required().messages({
                    'any.required': `"cidade" é um campo obrigatório`,
                    'string.empty': `"cidade" não deve ser vazio`
                }),
                zip: joi.number().required().messages({
                    'any.required': `"cep" é um campo obrigatório`,
                    'string.empty': `"cep" não deve ser vazio`
                }),
                address: joi.string().required().messages({
                    'any.required': `"endereço" é um campo obrigatório`,
                    'string.empty': `"endereço" não deve ser vazio`
                }),
                phone: joi.string().required().messages({
                    'any.required': `"telefone" é um campo obrigatório`,
                    'string.empty': `"telefone" não deve ser vazio`
                }),
            }, {
                allowUnknown: true,
            }),
            updateCliente
        )
        .delete(
            validateDTO('params', {
                id: joi.string().regex(/^[0-9a-fA-F]{24}$/).required().messages({
                    'any.required': `"categoria id" é um campo obrigatório`,
                    'string.empty': `"categoria id" não deve ser vazio`,
                    'string.regex': `"categoria id" fora do formato experado`,
                }),   
            }),
            removeCliente
        )
}