const joi =  require('joi')

const partnersCTRL = require(`../../controllers/partners.ctrl`)
const employeeCTRL = require(`../../controllers/employee.ctrl`)

const { createNewPartners, listAllPartners, listPartnersById,active , inactive, updatePartner, likesRecived } = partnersCTRL
const { createEmployee } = employeeCTRL

const Authorize = require('../../utils/middlewares/authorization.middleware')
const ValidateDTO = require('../../utils/middlewares/validate-dto.middleware')
const fileUploadMiddleware = require('../../utils/middlewares/file-upload.middleware')

module.exports = (Router) => {
    Router
        .route('/partners')
        .get(
            // Authorize('GET_ALL_PARTNER'),
            listAllPartners
            )
        .post(
            ValidateDTO('body', {
                name: joi.string().required().messages({
                    'any.required': `"Nome" é um campo obrigatório`,
                    'string.empty': `"Nome" não deve ser vazio`
                }),
                companyName: joi.string().required().messages({
                    'any.required': `"companyName" é um campo obrigatório`,
                    'string.empty': `"companyName" não deve ser vazio`
                }),
                address: joi.string().required().messages({
                    'any.required': `"endereco" é um campo obrigatório`,
                    'string.empty': `"endereco" não deve ser vazio`
                }),
                cnpj: joi.string().required().messages({
                    'any.required': `"cnpj" é um campo obrigatório`,
                    'string.empty': `"cnpj" não deve ser vazio`
                }),
                uf: joi.string().required().messages({
                    'any.required': `"uf" é um campo obrigatório`,
                    'string.empty': `"uf" não deve ser vazio`
                }),
                city: joi.string().required().messages({
                    'any.required': `"cidade" é um campo obrigatório`,
                    'string.empty': `"cidade" não deve ser vazio`
                }),
                accountable: joi.string().required().messages({
                    'any.required': `"responsavel" é um campo obrigatório`,
                    'string.empty': `"responsavel" não deve ser vazio`
                }),
                phone: joi.string().required().messages({
                    'any.required': `"telefone" é um campo obrigatório`,
                    'string.empty': `"telefone" não deve ser vazio`
                }),
                zip: joi.string().required().messages({
                    'any.required': `"telefone" é um campo obrigatório`,
                    'string.empty': `"telefone" não deve ser vazio`
                }),
                email: joi.string().required().messages({
                    'any.required': `"email" é um campo obrigatório`,
                    'string.empty': `"email" não deve ser vazio`
                }),
                password: joi.string().required().messages({
                    'any.required': `"password" é um campo obrigatório`,
                    'string.empty': `"password" não deve ser vazio`
                }), 
            }),

        createNewPartners
        )

    Router
        .route('/partner/:id')
        .get(
            Authorize('SEARCH_PARTNER_ID'),
            ValidateDTO('params', {
                id: joi.string().regex(/^[0-9a-fA-F]{24}$/).required().messages({
                    'any.required': `"categoria id" é um campo obrigatório`,
                    'string.empty': `"categoria id" não deve ser vazio`,
                    'string.regex': `"categoria id" fora do formato experado`,
                }),   
            }),
            listPartnersById
            )
        .put(
            fileUploadMiddleware('profile', true),
            ValidateDTO('params', {
                id: joi.string().regex(/^[0-9a-fA-F]{24}$/).required().messages({
                    'any.required': `"categoria id" é um campo obrigatório`,
                    'string.empty': `"categoria id" não deve ser vazio`,
                    'string.regex': `"categoria id" fora do formato experado`,
                }),   
            }),
            ValidateDTO('body', {
                name: joi.string().required().messages({
                    'any.required': `"Nome" é um campo obrigatório`,
                    'string.empty': `"Nome" não deve ser vazio`
                }),
                companyName: joi.string().required().messages({
                    'any.required': `"companyName" é um campo obrigatório`,
                    'string.empty': `"companyName" não deve ser vazio`
                }),
                address: joi.string().required().messages({
                    'any.required': `"endereco" é um campo obrigatório`,
                    'string.empty': `"endereco" não deve ser vazio`
                }),
                cnpj: joi.string().required().messages({
                    'any.required': `"cnpj" é um campo obrigatório`,
                    'string.empty': `"cnpj" não deve ser vazio`
                }),
                uf: joi.string().required().messages({
                    'any.required': `"uf" é um campo obrigatório`,
                    'string.empty': `"uf" não deve ser vazio`
                }),
                city: joi.string().required().messages({
                    'any.required': `"cidade" é um campo obrigatório`,
                    'string.empty': `"cidade" não deve ser vazio`
                }),
                zip: joi.string().required().messages({
                    'any.required': `"zip" é um campo obrigatório`,
                    'string.empty': `"zip" não deve ser vazio`
                }),
                accountable: joi.string().required().messages({
                    'any.required': `"responsavel" é um campo obrigatório`,
                    'string.empty': `"responsavel" não deve ser vazio`
                }),
                phone: joi.string().required().messages({
                    'any.required': `"telefone" é um campo obrigatório`,
                    'string.empty': `"telefone" não deve ser vazio`
                }),
                email: joi.string().required().messages({
                    'any.required': `"email" é um campo obrigatório`,
                    'string.empty': `"email" não deve ser vazio`
                }),
            }, {
                allowUnknown: true,
            }),
            updatePartner
        )
    Router
        .route('/partners/:id/active')
        .put(
            ValidateDTO('params', {
                id: joi.string().regex(/^[0-9a-fA-F]{24}$/).required().messages({
                    'any.required': `"categoria id" é um campo obrigatório`,
                    'string.empty': `"categoria id" não deve ser vazio`,
                    'string.regex': `"categoria id" fora do formato experado`,
                }),
            }),
            active
        )
    
    Router
        .route('/partners/:id/inactive')
        .put(
            ValidateDTO('params', {
                id: joi.string().regex(/^[0-9a-fA-F]{24}$/).required().messages({
                    'any.required': `"categoria id" é um campo obrigatório`,
                    'string.empty': `"categoria id" não deve ser vazio`,
                    'string.regex': `"categoria id" fora do formato experado`,
                }),
            }),
            inactive
        )
    
    Router
        .route('/partner/:id/createEmployee')
        // .get()
        .post(
            fileUploadMiddleware('profile', true),
            ValidateDTO('params', {
                id: joi.string().regex(/^[0-9a-fA-F]{24}$/).required().messages({
                    'any.required': `"categoria id" é um campo obrigatório`,
                    'string.empty': `"categoria id" não deve ser vazio`,
                    'string.regex': `"categoria id" fora do formato experado`,
                }),
            }),
            ValidateDTO('body', {
                name: joi.string().required().messages({
                    'any.required': `"Nome" é um campo obrigatório`,
                    'string.empty': `"Nome" não deve ser vazio`
                }),
                email: joi.string().required().messages({
                    'any.required': `"email" é um campo obrigatório`,
                    'string.empty': `"email" não deve ser vazio`
                }),
                password: joi.string().required().messages({
                    'any.required': `"password" é um campo obrigatório`,
                    'string.empty': `"password" não deve ser vazio`
                }),
                phone: joi.string().required().messages({
                    'any.required': `"telefone" é um campo obrigatório`,
                    'string.empty': `"telefone" não deve ser vazio`
                }),
                
                state: joi.string().required().messages({
                        'any.required': `"state" é um campo obrigatório`,
                        'string.empty': `"state" não deve ser vazio`
                }),
                city: joi.string().required().messages({
                        'any.required': `"city" é um campo obrigatório`,
                        'string.empty': `"city" não deve ser vazio`
                }),
                street: joi.string().required().messages({
                        'any.required': `"street" é um campo obrigatório`,
                        'string.empty': `"street" não deve ser vazio`
                }),
                zip: joi.string().required().messages({
                        'any.required': `"zip" é um campo obrigatório`,
                        'string.empty': `"zip" não deve ser vazio`
                }),
                
                categoryId: joi.string().regex(/^[0-9a-fA-F]{24}$/).required().messages({
                    'any.required': `"categoria id" é um campo obrigatório`,
                    'string.empty': `"categoria id" não deve ser vazio`,
                    'string.regex': `"categoria id" fora do formato experado`,
                }),
                status: joi.string().required().messages({
                    'any.required': `"status" é um campo obrigatório`,
                    'string.empty': `"status" não deve ser vazio`
                }),
            }, {
                allowUnknown: true,
            }),
            createEmployee
        )
    
    Router
        .route('/partner/:id/likes')
        // .get(
        //     ValidaDTO('params', {
        //         id: joi.string().regex(/^[0-9a-fA-F]{24}$/).required().messages({
        //           'any.required': `"partner id" é um campo obrigatório`,
        //           'string.empty': `"partner id" não deve ser vazio`,
        //           'string.pattern.base': `"partner id" fora do formato experado`,
        //         }),
        //     }),
        //     getLikes
        // )
        .post(
            Authorize('CREATE_LIKE'),
            ValidateDTO('params', {
                id: joi.string().regex(/^[0-9a-fA-F]{24}$/).required().messages({
                  'any.required': `"partner id" é um campo obrigatório`,
                  'string.empty': `"partner id" não deve ser vazio`,
                  'string.pattern.base': `"partner id" fora do formato experado`,
                }),
            }),
            likesRecived
        )
}

