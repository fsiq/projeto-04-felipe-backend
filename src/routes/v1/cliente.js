const clienteCTRL = require('../../controllers/cliente.ctrl') 
const joi = require('joi')

const Authorize = require('../../utils/middlewares/authorization.middleware')
const validateDTO = require('../../utils/middlewares/validate-dto.middleware')
const fileUploadMiddleware = require('../../utils/middlewares/file-upload.middleware')

const { findById, findLikes } = require('../../controllers/cliente.ctrl')

module.exports = (Router) => {
    Router
        .route('/cliente/:id')
        .get(
            validateDTO('params', {
                id: joi.string().regex(/^[0-9a-fA-F]{24}$/).required().messages({
                    'any.required': `"cliente id" é um campo obrigatório`,
                    'string.empty': `"cliente id" não deve ser vazio`,
                    'string.regex': `"cliente id" fora do formato experado`,
                }),          
            }),
            findById
        )
    Router
        .route('/cliente/:id/likes')
        .get(
            validateDTO('params', {
                id: joi.string().regex(/^[0-9a-fA-F]{24}$/).required().messages({
                    'any.required': `"cliente id" é um campo obrigatório`,
                    'string.empty': `"cliente id" não deve ser vazio`,
                    'string.regex': `"cliente id" fora do formato experado`,
                }),          
            }),
            findLikes
        )
        .post(
            validateDTO('params', {
                id: joi.string().regex(/^[0-9a-fA-F]{24}$/).required().messages({
                    'any.required': `"cliente id" é um campo obrigatório`,
                    'string.empty': `"cliente id" não deve ser vazio`,
                    'string.regex': `"cliente id" fora do formato experado`,
                }),
                id: joi.string().regex(/^[0-9a-fA-F]{24}$/).required().messages({
                    'any.required': `"cliente id" é um campo obrigatório`,
                    'string.empty': `"cliente id" não deve ser vazio`,
                    'string.regex': `"cliente id" fora do formato experado`,
                }),            
            }),
        )
}