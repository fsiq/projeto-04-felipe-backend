const joi = require('joi')
const employeeCTRL = require('../../controllers/employee.ctrl')
//
const validateDTO = require('../../utils/middlewares/validate-dto.middleware')
const Authorize = require('../../utils/middlewares/authorization.middleware')
const fileUploadMiddleware = require('../../utils/middlewares/file-upload.middleware')

const { listAllEmployee, listEmployeeById, createEventByEmployee, listEmployeeByFilter, deleteEmployee } = employeeCTRL 

module.exports = (Router) => {
    Router
        .route('/employee')
        .get(
            validateDTO('query', {
                categoryId: joi.string().regex(/^[0-9a-fA-F]{24}$/).messages({
                    'any.required': `"categoria id" é um campo obrigatório`,
                    'string.empty': `"categoria id" não deve ser vazio`,
                    'string.regex': `"categoria id" fora do formato experado`,
                }),
                partnerId: joi.string().regex(/^[0-9a-fA-F]{24}$/).messages({
                    'any.required': `"categoria id" é um campo obrigatório`,
                    'string.empty': `"categoria id" não deve ser vazio`,
                    'string.regex': `"categoria id" fora do formato experado`,
                }),
                nameLike: joi.string()
            }),
            listEmployeeByFilter
        )
    Router
        .route('/employee/:id')
        .get(
            validateDTO('params', {
                id: joi.string().regex(/^[0-9a-fA-F]{24}$/).required().messages({
                    'any.required': `"categoria id" é um campo obrigatório`,
                    'string.empty': `"categoria id" não deve ser vazio`,
                    'string.regex': `"categoria id" fora do formato experado`,
                }),          
            }),
            listEmployeeById
        )
    Router
        .route('/employee/:id/events')
        .get()
        .post(
            validateDTO('params', {
                id: joi.string().regex(/^[0-9a-fA-F]{24}$/).required().messages({
                    'any.required': `"categoria id" é um campo obrigatório`,
                    'string.empty': `"categoria id" não deve ser vazio`,
                    'string.regex': `"categoria id" fora do formato experado`,
                }),          
            }),
            validateDTO('body', {
                events: {
                    title: joi.string().required().messages({
                        'any.required': `"titulo" é um campo obrigatório`,
                        'string.empty': `"titulo" não deve ser vazio`
                    }),
                    description: joi.string().required().messages({
                        'any.required': `"descrição" é um campo obrigatório`,
                        'string.empty': `"descrição" não deve ser vazio`
                    }),

                    start: joi.string().required().messages({
                            'any.required': `"data de inicio" é um campo obrigatório`,
                            'string.empty': `"data de inicio" não deve ser vazio`
                    }),
                    end:joi.string().required().messages({
                        'any.required': `"data de termino" é um campo obrigatório`,
                        'string.empty': `"data de termino" não deve ser vazio`
                    })
                }
            }),
            createEventByEmployee
        )
        Router
            .route('/employee/:id/delete/:partnersID')
            .delete(
                Authorize('DELETE_EMPLOYEEE'),
                validateDTO('params', {
                    id: joi.string().regex(/^[0-9a-fA-F]{24}$/).required().messages({
                        'any.required': `"categoria id" é um campo obrigatório`,
                        'string.empty': `"categoria id" não deve ser vazio`,
                        'string.regex': `"categoria id" fora do formato experado`,
                    }),
                    partnersID: joi.string().regex(/^[0-9a-fA-F]{24}$/).required().messages({
                        'any.required': `"partners ID" é um campo obrigatório`,
                        'string.empty': `"partners ID" não deve ser vazio`,
                        'string.regex': `"partners ID" fora do formato experado`,
                    }),          
                }),
                deleteEmployee
            )
}