const partnersService = require('../services/partners.service')
const  { createNewPartner, listAllPartnerService, listPartnerById, changeStatus, updatePartnerService } = partnersService
const  { createLike } = require('../services/likes.service')
const createNewPartners = async (req, res , next) => {
    const { body } = req
    const resultService = await createNewPartner(body)
    const resultSucsses = resultService.sucsses ? 200 : 401;
    const resultData = resultService.sucsses ? {data: resultService.data} : {datails: resultService.details}
    return res.status (resultSucsses).send({
        message: resultService.message,
        ...resultData
    })
}

const listAllPartners = async (req, res, next) => {
    const data = await listAllPartnerService()
    res.status(200).send({
        data,
    })
}

const listPartnersById = async (req, res, next) => {
    const { id } = req.params
    
    const userId = req.user.id
    const kind = req.user.kindUser

    const resultService = await listPartnerById(id, userId , kind)

    const resultSucsses = resultService.sucsses ? 200 : 401;
    const resultData = resultService.sucsses ? {data: resultService.data} : {datails: resultService.details}
    
    return res.status(resultSucsses).send(resultData)
}

const active = async (req, res, next) => {
    const { id } = req.params 

    const resultService = await changeStatus(id, 'Ativo')

    const resultSucsses = resultService.sucsses ? 200 : 401;
    const resultData = resultService.sucsses ? {data: resultService.data} : {datails: resultService.details}
    return res.status (resultSucsses).send({
        message: 'operacão realizada com sucesso',
        ...resultData
    })
}

const inactive = async (req, res, next) => {
    const { id } = req.params 

    const resultService = await changeStatus(id, 'Inativo')

    const resultSucsses = resultService.sucsses ? 200 : 401;
    const resultData = resultService.sucsses ? {data: resultService.data} : {datails: resultService.details}
    return res.status (resultSucsses).send({
        message: 'operacão realizada com sucesso',
        ...resultData
    })
}

const updatePartner = async (req, res, next) => {
    const { id } = req.params
    const { body } = req
    
    const resultService = await updatePartnerService(id, body)

    const resultSucsses = resultService.sucsses ? 200 : 401;
    const resultData = resultService.sucsses ? {data: resultService.data} : {datails: resultService.details}
    
    return res.status(resultSucsses).send(resultData)
}

const likesRecived = async (req, res, next) => {
    const { params, user } = req
    
    const resultService = await createLike(params.id, user.id)

    const resultSucsses = resultService.sucsses ? 200 : 401;
    const resultData = resultService.sucsses ? {data: resultService.data} : {datails: resultService.details}
    
    return res.status(resultSucsses).send(resultData)
}

module.exports = {
    createNewPartners,
    listAllPartners,
    listPartnersById,
    active,
    inactive,
    updatePartner,
    likesRecived,
}