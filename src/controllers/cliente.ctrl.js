const clienteSercice= require('../services/cliente.service')

const { createNewCliente, listAllCliente, listClienteByid, updateClienteService, removeClienteService } = clienteSercice

const signUpCliente = async (req, res, next) => {
    const { body } = req
    const resultService = await createNewCliente(body)

    const resultSucsses = resultService.sucsses ? 200 : 401;
    const resultData = resultService.sucsses ? {data: resultService.data} : {datails: resultService.details}
    return res.status (resultSucsses).send({
        message: resultService.message,
        ...resultData
    })
}

const getAllCliente = async (req, res, next) => {
    const data = await listAllCliente()
    res.status(200).send({
        data,
    })
}

const findById = async (req, res, next) => {

}

const findLikes = async (req, res, next) => {

}

const accomplishLike = async (req, res, next) => {

}

const getClienteByid = async (req, res, next) => {
    const { id } = req.params
    const result = await listClienteByid(id)
    if(!result) {
        return res.status(404).send({
            details: [
                "user informado não existe"
            ]
        })
    }
    return res.status(200).send(result)
}  

const updateCliente = async (req, res, next) => {
    const { id } = req.params
    const { body } = req

    const resultService = await updateClienteService(id, body)

    const resultSucsses = resultService.sucsses ? 200 : 401;
    const resultData = resultService.sucsses ? {data: resultService.data} : {datails: resultService.details}
    
    return res.status(resultSucsses).send(resultData)
}

const removeCliente = async (req, res, next) => {
    const {id} = req.params

    const resultService = await removeClienteService(id)

    const resultSucsses = resultService.sucsses ? 200 : 401;
    const resultData = resultService.sucsses ? {data: resultService.data} : {datails: resultService.details}
    
    return res.status(resultSucsses).send(resultData)
}

module.exports = {
    signUpCliente,
    getAllCliente,
    getClienteByid,
    updateCliente,
    removeCliente,
    findById,
    findLikes,
    accomplishLike
}