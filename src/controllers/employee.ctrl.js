const employeeService = require('../services/employee.service')

const { listAllEmployeeService, listEmployeeByIdService,createEmployeeService, findByFilter, deleEmployeeService } = employeeService

const listAllEmployee = async (req, res, next) => {
    const resultService = await listAllEmployeeService()
    return res.status(200).send({ data: resultService })
}

const listEmployeeById = async (req, res, next) => {
    const { id } = req.params
    const resultService = await listEmployeeByIdService(id)
    if(!resultService) {
        return res.status(404).send({
            details: [
                "employee informado não existe"
            ]
        })
    }
    return res.status(200).send(resultService)
}

const listEmployeeByFilter = async (req, res, next) => {
    const { query } = req;
    const result = await findByFilter(query)
    return res.status(200).send({ data:  result })
}

const createEmployee = async (req, res, next) => {
    const { body, params } = req
    const resultService = await createEmployeeService({
        ...params,
        ...body
    })

    const resultSucsses = resultService.sucsses ? 200 : 400;
    const resultData = resultService.sucsses ? {data: resultService.data} : {datails: resultService.details}

    return res.status(resultSucsses).send(resultData)
}

const createEventByEmployee = async (req, res, next) => {
    const { body } = req
    const { id } = req.params

    const resultService = await createEventCalendarByEmployee(id,body)
}

const deleteEmployee =  async (req, res, next) => {
    const {id, partnersID} = req.params
    const userId = req.user.id
    
    const resultService = await deleEmployeeService(id, partnersID, userId)

    const resultSucsses = resultService.sucsses ? 200 : 400;
    const resultData = resultService.sucsses ? {data: resultService.data} : {datails: resultService.details}
    return res.status(resultSucsses).send(resultData)
}

module.exports = {
    listAllEmployee,
    listEmployeeByFilter,
    createEmployee,
    createEventByEmployee,
    listEmployeeById,
    deleteEmployee
}